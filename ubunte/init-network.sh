sudo cp /etc/netplan/50-cloud-init.yaml /etc/netplan/50-cloud-init.yaml.bak
sudo echo "
network:
    ethernets:
        ens33:
            addresses: [192.168.198.${1}/24]
            gateway4: 192.168.198.2
            nameservers:
                addresses: [192.168.198.2]
            dhcp4: true
    version: 2
" >> /etc/netplan/50-cloud-init.yaml 
sudo netplan apply
sudo ifconfig ens33 down
sudo ifconfig ens33 up 
