HOSTNAME="localhost"
PORT="3306"
USERNAME="root"
PASSWORD="a123456"

set -- $(getopt -q u:p: "$@")
if [ -n "$2" ]; then
        USERNAME=$2
fi
if [ -n "$4" ]; then
        PASSWORD=$4
fi

echo "create shell & scp"

echo "mysql -uroot -p${PASSWORD} << EOF" >> create_mysql_db.sh
if [ -n "$2" ]; then
        echo "create user ${USERNAME}@\"%\" identified by \"${PASSWORD}\";" >> create_mysql_db.sh
fi

echo "grant all privileges on *.* to ${USERNAME}@\"%\" identified by \"${PASSWORD}\" with grant option;" >> create_mysql_db.sh
echo "flush privileges;" >> create_mysql_db.sh
echo "EOF" >> create_mysql_db.sh

docker cp create_mysql_db.sh mysql:/home/create_mysql_db.sh
rm create_mysql_db.sh
docker exec mysql bash -c "chmod 777 /home/create_mysql_db.sh"
docker exec -u mysql mysql bash -c "sh /home/create_mysql_db.sh"

